import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Books from './components/Books.vue'
import Profile from './components/Profile.vue'
import Words from './components/Words.vue'
import Signin from './components/Signin.vue'
import Signup from './components/Signup.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
        path: '/',
        component: Home,
        name: 'home'
        },
        {
        path: '/books',
        name: 'books',
        component: Books
        },
        {
        path: '/profile',
        name: 'profile',
        component: Profile
        },
        {
        path: '/words',
        name: 'words',
        component: Words
        },
        {
        path: '/logout',
        name: 'logout',
        component: Home
        },
        {
        path: '/signin',
        name: 'signin',
        component: Signin
        },
        {
        path: '/signup',
        name: 'signup',
        component: Signup
        },
    ]
})
